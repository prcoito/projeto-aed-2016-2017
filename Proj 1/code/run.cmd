@echo off
Setlocal enableextensions enabledelayedexpansion
Color 0a
TITLE MENU
CHOICE /C 123 /M "Insira a implementacao a executar: "

IF ERRORLEVEL 3 (
	python.exe ./implementacao3/main.py
	GOTO End
)
IF ERRORLEVEL 2 (
	python.exe ./implementacao2/main.py
	GOTO End
)
IF ERRORLEVEL 1 (
	python.exe ./implementacao1/main.py
	GOTO End
)

echo.Inseriu uma opcao invalida
pause>nul

:End

