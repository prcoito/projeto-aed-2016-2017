﻿from classes import *
import csv
#import os
#import psutil
import time

def loadFile():    
    #inicio = time.clock()
    
    with open('../dados.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        row = next(reader)#ignora primeira linha do ficheiro csv
        
        arrayPais = []
        arraySigla = []
        for i in range(26):
            arraySigla.append(LinkedList())
            arrayPais.append(LinkedList())  
        
        ano_min = int(row[2])
        ano_max = int(row[len(row)-1])        
        
        for row in reader:
            nome = row[0]
            sigla = row[1]
            novoPais = Pais(nome, sigla)
            
            for i in range(2, len(row)):
                if(row[i]!=''):
                    novoPais.addDados((i-2+ano_min), row[i], 0)       #adiciona a arvore de dados(organizada por anos) do pais o valor da percentagem para aquele ano
                    novoPais.addDados((i-2+ano_min), row[i], 1)  #adiciona a arvore de dados(organizada por percentagens) do pais o valor da percentagem para aquele ano
            
            arraySigla[ord(sigla[0])-65].add(novoPais) # Se sigla pais começa letra A, vai para a lista de paises na pos 0 do array
                                                 # Se sigla pais começa letra B, vai para a lista de paises na pos 1 do array, e assim sucessivamente
            arrayPais[ord(nome[0])-65].add(novoPais)# Se nome pais começa letra A, vai para a lista de paises na pos 0 do array
                                                 # Se nome pais começa letra B, vai para a lista de paises na pos 1 do array, e assim sucessivamente
        #print(time.clock()*1000-inicio*1000) 
        return arraySigla, arrayPais, [ano_min, ano_max]
        
        
def resetRow(row):
    for i in range(len(row)):
        row[i] = ''
    return row

def updateFile(array, ano_min_max):
    #inicio = time.clock()
    ano_max = ano_min_max[1]
    ano_min = ano_min_max[0]
    
    with open('../dados.csv', "w", newline='') as csvfile:
        row = []
        for i in range(2+ano_max-ano_min+1):
            row.append('')
            
        writer = csv.writer(csvfile, delimiter=';' ,quotechar='', quoting=csv.QUOTE_NONE)
        row[0] = '"Country Name"'
        row[1] = '"Country Code"'
        
        for i in range(2, ano_max-ano_min+3):
            row[i] = '"'+str(ano_min-2+i)+'"'
        
        writer.writerow(row)
        
        for k in range(len(array)):
            
            ListaPais = array[k]
           
            nodePais = ListaPais.getData()
            row = resetRow(row)
            
            while nodePais != None:
                index = 1
                pais = nodePais.getData()
                
                row[0] = '"'+pais.getNome()+'"'
                row[1] = '"'+pais.getSigla()+'"'
            
                AVL = pais.getAVL()
                nodeAVL = AVL.getNode()
                
                while nodeAVL!=None:
                    dados = nodeAVL.getData()
                    ano = dados.getAno()
                   
                    row[ano-ano_min+2] = '"'+str(dados.getPercentagem())+'"'
                    AVL.delete(dados)
                    nodeAVL = AVL.getNode()
                    
                writer.writerow(row)
                row = resetRow(row)
                nodePais = nodePais.getNext()
            
    #print(time.clock()*1000 - inicio*1000)
    return

def inserirDadosPais(array, string, ano_min_max):    #string é uma palavra em letras maiusculas
    #inicio = time.clock()
    Linkedlist = array[ord(string[0])-65]
    node = Linkedlist.getData()
    
    while node!=None and node.getData().getNome().upper()!=string and node.getData().getSigla().upper()!=string:
        node = node.getNext()
    
    if node == None:
        print('Esse pais não existe. Nao foi possivel realizar a operação pretendida')
        return
    #parc = time.clock() - inicio
    ano = int(input('Insira o ano para o qual quer inserir informacao: '))
    if ano > ano_min_max[1] or ano < ano_min_max[0]:
        print('Esse ano ainda não existe. Utilize a opcao 2 do menu anterior para criar esse ano')
        return
    #inicio = time.clock()    
    invalido = True
    while invalido:
        #parc = parc + (time.clock() - inicio)
        percentagem = float(input('Insira a percentagem: '))
        if percentagem>=0 and percentagem<=100:
            invalido = False
        else:
            print('\nInseriu uma percentagem invalida')    
    #inicio = time.clock()
    node.getData().inserirDados(ano, percentagem)
    #print(time.clock()*1000 - inicio*1000 + parc*1000)
    return
    
def inserirInfoAnoPais(arraySigla, arrayPais, ano_min_max):
    op = str(input('Insira:\n1-Para escrever sigla do pais.\n2-Para escrever nome do pais.\n3-Para sair\n'))
    while(op!='3'):
        if(op == '1'):
            sigla = input('Insira a sigla: ')
            sigla = sigla.upper()
            inserirDadosPais(arraySigla, sigla, ano_min_max)
        elif(op == '2'):
            nome = input('Insira nome do pais: ')
            nome = nome.upper()
            inserirDadosPais(arrayPais, nome, ano_min_max)
        elif(op == '3'):
            break
        else:
            print('Inseriu uma opcao invalida\n')
        op = str(input('Insira:\n1-Para escrever sigla do pais.\n2-Para escrever nome do pais.\n3-Regressar ao menu anterior\n'))
        
    return

def inserirAnoTodosPaises(arraySigla, arrayPais, ano_min_max):
    ano = int(input('Insira o ano para o qual quer inserir informacao: '))
    if ano >= ano_min_max[0] and ano <= ano_min_max[1]:
        print('O ano inserido já exite')
    else:
        print('A percentagem para esse ano, em todos os paises, será 0 (zero).\nEsta pode ser mais tarde alterada\n')
        for i in arraySigla:
            nodePais = i.getData()
            while nodePais != None:
                nodePais.getData().addDados(ano, 0)
                nodePais = nodePais.getNext() 
                
        if ano < ano_min_max[0]:
            ano_min_max[0] = ano
        if ano > ano_min_max[1]:
            ano_min_max[1] = ano
                 
    return ano_min_max
    
def inserirDadosTodosPaises(arraySigla, arrayPais, ano_min_max):
    
    ano = int(input('Insira o ano para o qual quer inserir informacao: '))
    if ano > ano_min_max[1] or ano < ano_min_max[0]:
        op = str(input('Esse ano ainda não existe. Deseja cria-lo?\n1-Sim\n2-Não\n'))
        invalido = True
        while invalido:
            if op == '1':
                invalido = True
                while invalido:
                    percentagem = float(input('Insira a percentagem: '))
                    if percentagem>=0 and percentagem<=100:
                        invalido = False
                    else:
                        print('\nInseriu uma percentagem invalida')  
                        
                for i in arraySigla:
                    nodePais = i.getData()
                    while nodePais != None:
                        nodePais.getData().addDados(ano, percentagem, 0)
                        nodePais = nodePais.getNext()   
                if ano < ano_min_max[0]:
                    ano_min_max[0] = ano
                if ano > ano_min_max[1]:
                    ano_min_max[1] = ano
            elif op == '2':
                break
            else:
                print('Inseriu uma opcao invalida')
                op = int(input('Esse ano ainda não existe. Deseja cria-lo?\n\1-Sim\n2-Não\n'))
                    
    else:            
        invalido = True
        while invalido:
            percentagem = float(input('Insira a percentagem: '))
            if percentagem>=0 and percentagem<=100:
                invalido = False
            else:
                print('\nInseriu uma percentagem invalida')  
        #inicio = time.clock()        
        for i in arraySigla:
            nodePais = i.getData()
            while nodePais != None:
                nodePais.getData().addDados(ano, percentagem, 0)
                nodePais = nodePais.getNext()
        print('Dados inseridos com sucesso')
    #print(time.clock()*1000 - inicio*1000) 
    return ano_min_max

def inserirDados(arraySigla, arrayPais, ano_min_max):
    op = str(input('Que operacao pretender realizar?\n1-Inserir informacao de um ano para um pais.\n2-Criar um novo ano(para todos os paises)\n3-Inserir dados para todos os paises\n4-Regressar ao menu principal\n'))
    
    while op!='4':
        if op == '1':
            inserirInfoAnoPais(arraySigla, arrayPais, ano_min_max)
        elif op == '2':
            ano_min_max = inserirAnoTodosPaises(arraySigla, arrayPais, ano_min_max)
        elif op == '3':
            ano_min_max = inserirDadosTodosPaises(arraySigla, arrayPais, ano_min_max)
        elif op == '4':
            break
        else:
            print('Inseriu uma opcao errada')
        op = str(input('Que operacao pretender realizar?\n1-Inserir informacao de um ano para um pais.\n2-Inserir um novo ano(para todos os paises)\n3-Inserir dados para todos os paises\n4-Regressar ao menu principal\n'))
    
    return ano_min_max

def menu(arraySigla,arrayPais, ano_min_max):
    n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa\n2.Inserção\n3.Edição\n4.Remoção\n5.Listar países\n6.Sair\n"))
    while (n!='6'):
        if(n=='1'):
            searchMenu(arraySigla,arrayPais, ano_min_max)
        elif(n=='2'):
            ano_min_max = inserirDados(arraySigla, arrayPais, ano_min_max)
        elif(n=='3'):
            editar(arraySigla, arrayPais, ano_min_max)
        elif(n=='4'):
            remover(arraySigla,arrayPais, ano_min_max)
        elif(n=='5'):
            listarP(arrayPais)
        else:
            print("Opção invalida")
        n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa\n2.Inserção\n3.Edição\n4.Remoção\n5.Listar países\n6.Sair\n"))
    
    return ano_min_max


def searchMenu(arraySigla, arrayPais, ano_min_max):
    n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa por país\n2.Pesquisa por ano\n3.Pesquisar por sigla\n4.Voltar para o menu principal\n"))
    while (n!='4'):
        if(n=='1'):
            searchC(arrayPais)
        elif(n=='2'):
            searchY(arrayPais, ano_min_max)
        elif(n=='3'):
            searchS(arraySigla)
        else:
            print("Opção invalida\n") 
        n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa por país\n2.Pesquisa por ano\n3.Pesquisar por sigla\n4.Voltar para o menu principal\n"))
    return

def searchC(arrayPais):
    nome=str(input("Introduza o nome do país que pretende pesquisar: "))
    
    #inicio = time.clock()
    if not nome[0].upper() >='A' and nome[0].upper() <= 'Z':
        print("País inválido") 
        return
    
    cLetter = arrayPais[ord(nome[0].upper())-65].getData()

    while (cLetter!=None): #caso se chegue ao fim
        if(cLetter.getData().getNome().lower()==nome.lower()):
            datalist=cLetter.getData().getAVL()
            datalist.display()
            #print(time.clock()*1000-inicio*1000)
            return
        cLetter=cLetter.getNext() #avança para outro no (pais)        
    return

def searchY(arrayPais, ano_min_max):
    n=int(input("Introduza o ano que quer pesquisar: "))
    
    #inicio = time.clock()
    if n>ano_min_max[1] or n<ano_min_max[0]:
        print('Ano inserido invalido')
        return
    
    for i in arrayPais:
        npais=i.getData()
        while (npais!=None):
            dado=npais.getData().getAVL()
            boolean, nodeTree = dado.search(Dados(n, 0))
            if boolean==True:
                print ("País: " + npais.getData().getNome() + "--------Percentagem no ano pedido: " +  str(nodeTree.getData().getPercentagem()))
            npais=npais.getNext()
    
    #print(time.clock()*1000-inicio*1000)
    return


def searchS(arraySigla):
    sigla=str(input("Introduz o sigla do país que pretende pesquisar: "))
    
    #inicio = time.clock()
    
    if not sigla[0].upper() >='A' and sigla[0].upper() <= 'Z':
        print("Sigla inválida") 
        return
    
    sLetter = arraySigla[ord(sigla[0].upper())-65].getData()

    while (sLetter!=None): #caso se chegue ao fim
        if(sLetter.getData().getSigla().lower()==sigla.lower()):
            datalist=sLetter.getData().getAVL()
            datalist.display()
            #print(time.clock()*1000-inicio*1000)
            return
        sLetter=sLetter.getNext() #avança para outro no (pais) 
    return

def remover(arraySigla,arrayPais, ano_min_max):
    n=str(input("Introduza o número da ação que deseja executar:\n1.Remover uma percentagem de um país num ano\n2.Remover uma percentagem de um país de todos os anos\n3.Remover uma percentagem de todos os países num ano\n4.Voltar para o menu principal\n"))
    while (n!='4'):
        if(n=='1'):
            removepa(arrayPais, ano_min_max)
        elif(n=='2'):
            removepta(arrayPais, ano_min_max)
        elif(n=='3'):
            removetpa(arraySigla, ano_min_max)
        else:
            n=str(input("Introduza uma opção valida"))
        n=str(input("Introduza o número da ação que deseja executar:\n1.Remover uma percentagem de um país num ano\n2.Remover uma percentagem de um país de todos os anos\n3.Remover uma percentagem de todos os países num ano\n4.Voltar para o menu principal\n"))
    return

def removepa(arrayPais, ano_min_max):
    nome=str(input("Introduza o nome do país que pretende pesquisar: "))
    
    if not nome[0].upper() >='A' and nome[0].upper() <= 'Z':
        print("País inválido") 
        return
    
    cLetter = arrayPais[ord(nome[0].upper())-65].getData()
    
    ano=int(input("Escolha o ano: "))
    
    if ano>ano_min_max[1] or ano<ano_min_max[0]:
            print('Ano inserido invalido')
            return    

    while (cLetter!=None): #caso se chegue ao fim
        #Ciclo para correr todos os anos
        if(cLetter.getData().getNome().lower()==nome.lower()) :
            print("Encontrou  o pais inserido")
            avlPais = cLetter.getData().getAVL()
            
            boolean, nodeTree = avlPais.search(Dados(ano, 0))
            if boolean == False:
                print('Nao foi possivel remover informação desse ano porque não existe')
            else:
                avlPais.delete(nodeTree.getData())
                print('Informação removida com sucesso')
            return
        cLetter=cLetter.getNext() #avança para outro no (pais)        
    return

def removepta(arrayPais, ano_min_max): #Remover uma percentagem de um país de todos os anos
    nome=str(input("Introduza o nome do país que pretende pesquisar: "))
    
    if not nome[0].upper() >='A' and nome[0].upper() <= 'Z':
        print("País inválido") 
        return
    
    cLetter = arrayPais[ord(nome[0].upper())-65].getData()
    

    while (cLetter!=None): #caso se chegue ao fim
        #Ciclo para correr todos os anos
        if(cLetter.getData().getNome().lower()==nome.lower()) :
            print("Encontrou  o pais inserido")
            avlPais = cLetter.getData().getAVL()
            #remover todos os anos
            while avlPais.getNode() != None:
                avlPais.delete(avlPais.getNode().getData())
            print('Remoção completa')
            return
        cLetter=cLetter.getNext() #avança para outro no (pais)   
    return


def removetpa(arrayPais, ano_min_max):    #Remover uma percentagem de todos os países num ano
    ano=int(input("Escolha o ano: "))
    
    if ano>ano_min_max[1] or ano<ano_min_max[0]:
            print('Ano inserido invalido')
            return    
    for i in range(0,26):
        cLetter = arrayPais[i].getData()
        while (cLetter!=None): #caso se chegue ao fim
            #Ciclo para correr todos os anos
            avlPais = cLetter.getData().getAVL()
                
            boolean, nodeTree = avlPais.search(Dados(ano, 0))
            if boolean == True:
                avlPais.delete(nodeTree.getData())        
            cLetter=cLetter.getNext() #avança para outro no (pais) 
    print('Remoção completa')
    return

def editar(arraySigla, arrayPais, ano_min_max):
    op=str(input("Introduza o número da ação que deseja executar:\n1.Introduzir sigla do pais\n2.Introduzir nome do país\n3.Voltar para o menu anterior\n"))
    while (op != '3'):
        if(op == '1'):
            sigla = str(input('Insira a sigla: '))
            sigla = sigla.upper()
            if not sigla[0] >='A' and sigla[0] <= 'Z':
                print("País inválido") 
            else:
                editarDadosSigla(arraySigla, sigla, ano_min_max)
        elif(op == '2'):
            nome = str(input('Insira nome do pais: '))
            nome = nome.upper()
            if not nome[0] >='A' and nome[0] <= 'Z':
                print("País inválido") 
            else:
                editarDadosPais(arrayPais, nome, ano_min_max)
        else:
            print("Opção invalida")
        op=str(input("Introduza o número da ação que deseja executar:\n1.Introduzir sigla do pais\n2.Introduzir nome do país\n3.Voltar para o menu anterior\n"))
    
    
def editarDadosPais(arrayPais, nome, ano_min_max):  # nome em maiusculas
    
    nodePais = arrayPais[ord(nome[0])-65].getData()
    
    while nodePais!= None:  #procura pais na lista ligada
        if nodePais.getData().getNome().upper() == nome.upper():
            break
        nodePais = nodePais.getNext()
        
    if nodePais == None:
        print('Pais inserido nao existe')
        return
    
    ano = int(input("Qual o ano que quer editar? "))
    if ano< ano_min_max[0] or ano > ano_min_max[1]:
        print('Ano inserido invalido')
        return
    
    boolean, dados = nodePais.getData().getAVL().search(Dados(ano, 0)) # verifica se input é valido
    if boolean == False:    # se nao for mostra erro
        print('Nao existe informacao a editar nesse ano para esse pais')
        return     
    
    invalido = True
    while invalido:
        percentagem = float(input("Novo Valor de percentagem: "))
        if percentagem>=0 and percentagem <=100:
            invalido = False
        else:
            print('Percentagem inserida invalida')
    
    dados.getData().editarPercentagem(percentagem)  # atualiza percentagem
    return

def editarDadosSigla(arrayPais, sigla, ano_min_max):  # sigla em maiusculas
    
    nodePais = arrayPais[ord(sigla[0])-65].getData()
    
    while nodePais!= None:  #procura pais na lista ligada
        if nodePais.getData().getSigla().upper() == sigla.upper():
            break
        nodePais = nodePais.getNext()
        
    if nodePais == None:
        print('Pais inserido nao existe')
        return
    
    ano = int(input("Qual o ano que quer editar? "))
    if ano< ano_min_max[0] or ano > ano_min_max[1]:
        print('Ano inserido invalido')
        return
    
    boolean, dados = nodePais.getData().getAVL().search(Dados(ano, 0)) # verifica se input é valido
    if boolean == False:    # se nao for mostra erro
        print('Nao existe informacao a editar nesse ano para esse pais')
        return     
    
    invalido = True
    while invalido:
        percentagem = float(input("Novo Valor de percentagem: "))
        if percentagem>=0 and percentagem <=100:
            invalido = False
        else:
            print('Percentagem inserida invalida')
    
    dados.getData().editarPercentagem(percentagem)  # atualiza percentagem
    return

def listarP(arrayPais):
    #inicio = time.clock()
    for listp in arrayPais:
        listp.print_list()
    #print(time.clock()*1000 - inicio*1000)
    
def main():
    arraySigla, arrayPais, ano_min_max = loadFile()
    
    #process = psutil.Process(os.getpid())
    #print(process.memory_info().rss/1024/1024)
    
    ano_min_max = menu(arraySigla, arrayPais, ano_min_max)
    
    updateFile(arraySigla, ano_min_max)
    
if __name__ == "__main__":
    main()