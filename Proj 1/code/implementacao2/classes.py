﻿class Pais:
    def __init__(self, nome, sigla):
        self.nome = nome
        self.sigla = sigla
        self.AVL = AVLTree()
        self.AVL_P = AVLTree()
        
    def addDados(self, ano, percentagem, modo):
        if modo == 0:
            self.AVL.insert(Dados(ano, percentagem), 0)
        else:
            self.AVL_P.insert(Dados(ano, percentagem), 1)
        return
    
    def removeDados(self, ano, percentagem):
        return
    
    def inserirDados(self, ano, percentagem):
        boolean, nodeTree = self.getAVL().search(Dados(ano, 0))
        
        if boolean == False:
            self.addDados(ano, percentagem, 0)
        else:
            invalido = True
            print('Erro em',self.getNome())
            op = str(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
            while invalido:
                if op == '1':
                    invalido = False
                    nodeTree.getData().editarPercentagem(percentagem)
                    return
                elif op == '2':
                    invalido = False
                    return
                else:
                    print('Inseriu uma opção inválida')
                op = str(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
        return
    
    def getNome(self):
        return self.nome
    
    def getSigla(self):
        return self.sigla
    
    def getAVL(self):
        return self.AVL
    
    def getAVLPercentagem(self):
        return self.AVL_P
    
    def getClassName(self):
        return "Pais"
    
    def __str__(self):
        return self.getNome()+" "+self.getSigla() 
    
class Dados:
    def __init__(self, ano, percentagem):
        self.ano = ano
        self.percentagem = percentagem
        
    def getAno(self):
        return self.ano
    
    def getPercentagem(self):
        return self.percentagem
    
    def editarPercentagem(self, nova):
        self.percentagem = nova
    
    def getClassName(self):
        return "Dados"
    
    def __str__(self):
        return str(self.getAno())+"->"+str(self.getPercentagem())        
    
    def __lt__(self, other):
        return self.getAno() < other.getAno()
    
    def __le__(self, other):
        return self.getAno() <= other.getAno()
    
    def __eq__(self, other):
        return self.getAno() == other.getAno()
    
    def __ne__(self, other):
        return not self.__eq__()
    
    def __gt__(self, other):
        return self.getAno() > other.getAno()
    
    def __ge__(self, other):
        return self.getAno() >= other.getAno()       
   
#@source: https://github.com/recluze/python-avl-tree/blob/master/simple_avl.py

outputdebug = False 

def debug(msg):
    if outputdebug:
        print (msg)

class NodeTree():
    def __init__(self, key):
        self.key = key
        self.left = None 
        self.right = None
        
    def getData(self):
        return self.key
    
    def getLeft(self):
        return self.left
    
    def getRight(self):
        return self.right
    

class AVLTree():
    def __init__(self, *args):
        self.node = None 
        self.height = -1  
        self.balance = 0; 
        
        if len(args) == 1: 
            for i in args[0]: 
                self.insert(i)
     
    def getNode(self):
        return self.node
    
    def height(self):
        if self.node: 
            return self.node.height 
        else: 
            return 0 
    
    def is_leaf(self):
        return (self.height == 0) 
    
    def search(self, item):
        if self.getNode() == None:
            return False, 0

        if item == self.getNode().getData():
            #--------------------------------------------ALTEREI---------------------------
            return True, self.getNode()
        else:
            if item < self.getNode().getData() :
                return self.getNode().getLeft().search(item)
            else:
                return self.getNode().getRight().search(item)
            
    def searchP(self, item):
        if self.getNode() == None:
            return False, 0

        if item.getPercentagem() == self.getNode().getData().getPercentagem():
            return True, self.getNode()
        else:
            if item.getPercentagem() < self.getNode().getData().getPercentagem():
                return self.getNode().getLeft().search(item)
            else:
                return self.getNode().getRight().search(item)    
        
    def insert(self, key, modo):
        tree = self.node
        
        if(key.getClassName()=='Pais'):
            
            newnode = NodeTree(key)
            
            if tree == None:
                self.node = newnode 
                self.node.left = AVLTree() 
                self.node.right = AVLTree()
                debug("Inserted key [" + str(key) + "]")
            
            elif key.getNome() < tree.key.getNome(): 
                self.node.left.insert(key, modo)
                
            elif key.getNome() > tree.key.getNome(): 
                self.node.right.insert(key, modo)
            
            else: 
                debug("Key [" + str(key) + "] already in tree.")
                
            self.rebalance() 
        else:
            if modo == 0:
                newnode = NodeTree(key)
                
                if tree == None:
                    self.node = newnode 
                    self.node.left = AVLTree() 
                    self.node.right = AVLTree()
                    debug("Inserted key [" + str(key) + "]")
                
                elif key.getAno() < tree.key.getAno(): 
                    self.node.left.insert(key, modo)
                    
                elif key.getAno() > tree.key.getAno(): 
                    self.node.right.insert(key, modo)
                
                else: 
                    debug("Key [" + str(key) + "] already in tree.")
            else:
                newnode = NodeTree(key)
                
                if tree == None:
                    self.node = newnode 
                    self.node.left = AVLTree() 
                    self.node.right = AVLTree()
                    debug("Inserted key [" + str(key) + "]")
                
                elif key.getPercentagem() < tree.key.getPercentagem(): 
                    self.node.left.insert(key, modo)
                    
                elif key.getPercentagem() > tree.key.getPercentagem(): 
                    self.node.right.insert(key, modo)
                
                else: 
                    debug("Key [" + str(key) + "] already in tree.")                
                
            self.rebalance()             
            
        
    def rebalance(self):
        ''' 
        Rebalance a particular (sub)tree
        ''' 
        # key inserted. Let's check if we're balanced
        self.update_heights(False)
        self.update_balances(False)
        while self.balance < -1 or self.balance > 1: 
            if self.balance > 1:
                if self.node.left.balance < 0:  
                    self.node.left.lrotate() # we're in case II
                    self.update_heights()
                    self.update_balances()
                self.rrotate()
                self.update_heights()
                self.update_balances()
                
            if self.balance < -1:
                if self.node.right.balance > 0:  
                    self.node.right.rrotate() # we're in case III
                    self.update_heights()
                    self.update_balances()
                self.lrotate()
                self.update_heights()
                self.update_balances()


            
    def rrotate(self):
        # Rotate left pivoting on self
        debug ('Rotating ' + str(self.node.key) + ' right') 
        A = self.node 
        B = self.node.left.node 
        T = B.right.node 
        
        self.node = B 
        B.right.node = A 
        A.left.node = T 

    
    def lrotate(self):
        # Rotate left pivoting on self
        debug ('Rotating ' + str(self.node.key) + ' left') 
        A = self.node 
        B = self.node.right.node 
        T = B.left.node 
        
        self.node = B 
        B.left.node = A 
        A.right.node = T 
        
            
    def update_heights(self, recurse=True):
        if not self.node == None: 
            if recurse: 
                if self.node.left != None: 
                    self.node.left.update_heights()
                if self.node.right != None:
                    self.node.right.update_heights()
            
            self.height = max(self.node.left.height,
                              self.node.right.height) + 1 
        else: 
            self.height = -1 
            
    def update_balances(self, recurse=True):
        if not self.node == None: 
            if recurse: 
                if self.node.left != None: 
                    self.node.left.update_balances()
                if self.node.right != None:
                    self.node.right.update_balances()

            self.balance = self.node.left.height - self.node.right.height 
        else: 
            self.balance = 0 

    def delete(self, key):
        # debug("Trying to delete at node: " + str(self.node.key))
        if self.node != None: 
            if self.node.key == key: 
                debug("Deleting ... " + str(key))  
                if self.node.left.node == None and self.node.right.node == None:
                    self.node = None # leaves can be killed at will 
                # if only one subtree, take that 
                elif self.node.left.node == None: 
                    self.node = self.node.right.node
                elif self.node.right.node == None: 
                    self.node = self.node.left.node
                
                # worst-case: both children present. Find logical successor
                else:  
                    replacement = self.logical_successor(self.node)
                    if replacement != None: # sanity check 
                        debug("Found replacement for " + str(key) + " -> " + str(replacement.key))  
                        self.node.key = replacement.key 
                        
                        # replaced. Now delete the key from right child 
                        self.node.right.delete(replacement.key)
                    
                self.rebalance()
                return  
            elif key < self.node.key: 
                self.node.left.delete(key)  
            elif key > self.node.key: 
                self.node.right.delete(key)
                        
            self.rebalance()
        else: 
            return 

    def logical_predecessor(self, node):
        ''' 
        Find the biggest valued node in LEFT child
        ''' 
        node = node.left.node 
        if node != None: 
            while node.right != None:
                if node.right.node == None: 
                    return node 
                else: 
                    node = node.right.node  
        return node 
    
    def logical_successor(self, node):
        ''' 
        Find the smallese valued node in RIGHT child
        ''' 
        node = node.right.node  
        if node != None: # just a sanity check  
            
            while node.left != None:
                debug("LS: traversing: " + str(node.key))
                if node.left.node == None: 
                    return node 
                else: 
                    node = node.left.node  
        return node 

    def check_balanced(self):
        if self == None or self.node == None: 
            return True
        
        # We always need to make sure we are balanced 
        self.update_heights()
        self.update_balances()
        return ((abs(self.balance) < 2) and self.node.left.check_balanced() and self.node.right.check_balanced())  
        
    def inorder_traverse(self):
        if self.node == None:
            return [] 
        
        inlist = [] 
        l = self.node.left.inorder_traverse()
        for i in l: 
            inlist.append(i) 

        inlist.append(self.node.key)

        l = self.node.right.inorder_traverse()
        for i in l: 
            inlist.append(i) 
    
        return inlist 

    def display(self):
        '''
        Display the whole tree. Uses recursive def.
        TODO: create a better display using breadth-first search
        '''        
        self.update_heights()  # Must update heights before balances 
        self.update_balances()
        if(self.node != None): 
            print (self.node.getData())
            if self.node.left != None: 
                self.node.left.display()
            if self.node.left != None:
                self.node.right.display()
                
class Node:
    def __init__(self, init_data):
        self.data = init_data
        self.next = None
        
    def getData(self):
        return self.data
    
    def getNext(self):
        return self.next
    
    def setData(self, new_data):
        self.data = new_data
        
    def setNext(self, new_next):
        self.next = new_next   
        
class LinkedList:
    def __init__(self):
        self.head = None
        
    def getData(self):
        return self.head
    
    def is_empty(self):
        return self.head == None
    
    def add(self, item):
        current = self.head
        previous = None
        stop = False
        if(item.getClassName() == 'Pais'):
            while current != None:
                previous = current
                current = current.getNext()
        
            temp = Node(item)
            if previous == None:
                temp.setNext(self.head)
                self.head = temp
            else:
                temp.setNext(current)
                previous.setNext(temp)            
        else:
            while current != None and not stop:
                if current.getData().getAno() > item.getAno():
                    stop = True
                else:
                    previous = current
                    current = current.getNext()
        
            temp = Node(item)
            if previous == None:
                temp.setNext(self.head)
                self.head = temp
            else:
                temp.setNext(current)
                previous.setNext(temp)
                
    def remove(self, item):
        current = self.head
        previous = None
        found = False
        while not found:
            if current.getData() == item:
                found = True
            else:
                previous = current
                current = current.getNext()
                if previous == None:
                    self.head = current.getNext()
                else:
                    previous.set_next(current.getNext())  

    def print_list(self):
        currentNode = self.head
       
        while currentNode != None:
            print(currentNode.getData())
            currentNode = currentNode.getNext()
            
        
    def size(self):
        currentNode = self.head
        l = 0
              
        while currentNode != None:
            l = l+1
            currentNode = currentNode.getNext()
        return l
    
    def search(self, item):
        currentNode = self.head
        while currentNode!=None:
            if(currentNode.getData() == item):
                return True
            else:
                currentNode = currentNode.getNext()
        return False                