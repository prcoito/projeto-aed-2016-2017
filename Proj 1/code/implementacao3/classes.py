﻿class Pais:
    def __init__(self, nome, sigla):
        self.nome = nome
        self.sigla = sigla
        self.AVL = AVLTree()
        
    def addDados(self, ano, percentagem):
        self.AVL.insert(Dados(ano, percentagem), 0)
        return
    
    def removeDados(self, ano, percentagem):
        return
    
    def inserirDados(self, ano, percentagem):
        boolean, dados = self.getAVL().search(Dados(ano, 0), 0)
        
        if boolean == False:
            self.addDados(ano, percentagem)
        else:
            invalido = True
            print('Erro em',self.getNome())
            op = int(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
            while invalido:
                if op == 1:
                    invalido = False
                    dados.editarPercentagem(percentagem)
                elif op == 2:
                    invalido = False
                else:
                    print('Inseriu uma opção inválida')
        return
    
    def getNome(self):
        return self.nome
    
    def getSigla(self):
        return self.sigla
    
    def getAVL(self):
        return self.AVL
    
    def getClassName(self):
        return "Pais"
    
    def __str__(self):
        return self.getNome()+" "+self.getSigla() 
    
    def __eq__(self, other):
        return self.getNome().upper() == other.getNome().upper()# or self.getSigla() == other.getSigla()
    
    def __lt__(self, other):
        return self.getNome().upper() < other.getNome().upper()
    
    def __le__(self, other):
        return self.getNome().upper() <= other.getNome().upper()
    
    def __ne__(self, other):
        return not self.__eq__()
    
    def __gt__(self, other):
        return self.getNome().upper() > other.getNome().upper()
    
    def __ge__(self, other):
        return self.getNome().upper() >= other.getNome().upper()      
    
class Dados:
    def __init__(self, ano, percentagem):
        self.ano = ano
        self.percentagem = percentagem
        
    def getAno(self):
        return self.ano
    
    def getPercentagem(self):
        return self.percentagem
    
    def editarPercentagem(self, nova):
        self.percentagem = nova
    
    def getClassName(self):
        return "Dados"
    
    def __str__(self):
        return str(self.getAno())+"->"+str(self.getPercentagem())        
    
    def __lt__(self, other):
        return self.getAno() < other.getAno()
    
    def __le__(self, other):
        return self.getAno() <= other.getAno()
    
    def __eq__(self, other):
        return self.getAno() == other.getAno()
    
    def __ne__(self, other):
        return not self.__eq__()
    
    def __gt__(self, other):
        return self.getAno() > other.getAno()
    
    def __ge__(self, other):
        return self.getAno() >= other.getAno()       
   
#@source: https://github.com/recluze/python-avl-tree/blob/master/simple_avl.py

outputdebug = False 

def debug(msg):
    if outputdebug:
        print (msg)

class NodeTree():
    def __init__(self, key):
        self.key = key
        self.left = None 
        self.right = None
        
    def getData(self):
        return self.key
    
    def getLeft(self):
        return self.left
    
    def getRight(self):
        return self.right
    
    

class AVLTree():
    def __init__(self, *args):
        self.node = None 
        self.height = -1  
        self.balance = 0; 
        
        if len(args) == 1: 
            for i in args[0]: 
                self.insert(i)
     
    def getNode(self):
        return self.node
    
    def height(self):
        if self.node: 
            return self.node.height 
        else: 
            return 0 
    
    def is_leaf(self):
        return (self.height == 0) 
    
    def search(self, item, modo): # modo 1 SÓ é para a procura de paises por SIGLA
        
        if self.getNode() == None:
            return False, 0
        
        if self.getNode().getData().getClassName() == 'Pais':
            if modo == 0:
                if item == self.getNode().getData():
                    return True, self.getNode().getData()   # devolve pais
                
                if item < self.getNode().getData() :
                    return self.getNode().getLeft().search(item, modo)
                else:
                    return self.getNode().getRight().search(item, modo)                    
            else:      #modo 1 == pesquisa sigla
                if item.getSigla().upper() == self.getNode().getData().getSigla().upper():
                    return True, self.getNode().getData()            
                
                if item.getSigla().upper() < self.getNode().getData().getSigla().upper():
                    return self.getNode().getLeft().search(item, modo)
                else:
                    return self.getNode().getRight().search(item, modo)                       
                
        else:
            if item == self.getNode().getData():
                return True, self.getNode().getData() #devolve dados
            
            if item < self.getNode().getData() :
                return self.getNode().getLeft().search(item, modo)
            else:
                return self.getNode().getRight().search(item, modo)
        
    def inserirTodosPaises(self, item):
        if self.getNode() == None:
            return True
        else:
            boolean, dados = self.getNode().getData().getAVL().search(item, 0)
            if boolean == True:
                invalido = True
                print('Erro em',self.getNode().getData())
                op = str(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
                while invalido:
                    if op == '1':
                        invalido = False
                        dados.editarPercentagem(item.getPercentagem())
                        return
                    elif op == '2':
                        invalido = False
                        return 
                    else:
                        print('Inseriu uma opção inválida')
                    op = str(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
            else:
                self.getNode().getData().getAVL().insert(item, 0)
            return self.getNode().getLeft().inserirTodosPaises(item) and self.getNode().getRight().inserirTodosPaises(item)
    
    def removerInfoTodosPaises(self, item):
        if self.getNode() == None:
            return True
        else:
            boolean, dados = self.getNode().getData().getAVL().search(item, 0)
            if boolean == True:
                self.getNode().getData().getAVL().delete(dados, 0)
            return self.getNode().getLeft().removerInfoTodosPaises(item) and self.getNode().getRight().removerInfoTodosPaises(item)
            
    def imprimirDadosAno(self, item):
        if self.getNode() == None:
            return True
        else:
            boolean, dados = self.getNode().getData().getAVL().search(item, 0)
            if boolean == True:
                print ("País: " + self.getNode().getData().getNome() + "--------Percentagem no ano pedido: " +  str(dados.getPercentagem()))
            return self.getNode().getLeft().imprimirDadosAno(item) and self.getNode().getRight().imprimirDadosAno(item)
        
    def insert(self, key, modo):
        tree = self.node
        
        if(key.getClassName()=='Pais'):
            
            if modo == 0:
                newnode = NodeTree(key)
                
                if tree == None:
                    self.node = newnode 
                    self.node.left = AVLTree() 
                    self.node.right = AVLTree()
                    debug("Inserted key [" + str(key) + "]")
                
                elif key.getNome() < tree.key.getNome(): 
                    self.node.left.insert(key, 0)
                    
                elif key.getNome() > tree.key.getNome(): 
                    self.node.right.insert(key, 0)
                
                else: 
                    debug("Key [" + str(key) + "] already in tree.")
            else:
                newnode = NodeTree(key)
                                
                if tree == None:
                    self.node = newnode 
                    self.node.left = AVLTree() 
                    self.node.right = AVLTree()
                    debug("Inserted key [" + str(key) + "]")
                
                elif key.getSigla() < tree.key.getSigla(): 
                    self.node.left.insert(key, 1)
                    
                elif key.getSigla() > tree.key.getSigla(): 
                    self.node.right.insert(key, 1)
                
                else: 
                    debug("Key [" + str(key) + "] already in tree.")                
                
                
            self.rebalance() 
        else:
            newnode = NodeTree(key)
            
            if tree == None:
                self.node = newnode 
                self.node.left = AVLTree() 
                self.node.right = AVLTree()
                debug("Inserted key [" + str(key) + "]")
            
            elif key.getAno() < tree.key.getAno(): 
                self.node.left.insert(key, 0)
                
            elif key.getAno() > tree.key.getAno(): 
                self.node.right.insert(key, 0)
            
            else: 
                debug("Key [" + str(key) + "] already in tree.")
                
            self.rebalance()             
            
        
    def rebalance(self):
        ''' 
        Rebalance a particular (sub)tree
        ''' 
        # key inserted. Let's check if we're balanced
        self.update_heights(False)
        self.update_balances(False)
        while self.balance < -1 or self.balance > 1: 
            if self.balance > 1:
                if self.node.left.balance < 0:  
                    self.node.left.lrotate() # we're in case II
                    self.update_heights()
                    self.update_balances()
                self.rrotate()
                self.update_heights()
                self.update_balances()
                
            if self.balance < -1:
                if self.node.right.balance > 0:  
                    self.node.right.rrotate() # we're in case III
                    self.update_heights()
                    self.update_balances()
                self.lrotate()
                self.update_heights()
                self.update_balances()


            
    def rrotate(self):
        # Rotate left pivoting on self
        debug ('Rotating ' + str(self.node.key) + ' right') 
        A = self.node 
        B = self.node.left.node 
        T = B.right.node 
        
        self.node = B 
        B.right.node = A 
        A.left.node = T 

    
    def lrotate(self):
        # Rotate left pivoting on self
        debug ('Rotating ' + str(self.node.key) + ' left') 
        A = self.node 
        B = self.node.right.node 
        T = B.left.node 
        
        self.node = B 
        B.left.node = A 
        A.right.node = T 
        
            
    def update_heights(self, recurse=True):
        if not self.node == None: 
            if recurse: 
                if self.node.left != None: 
                    self.node.left.update_heights()
                if self.node.right != None:
                    self.node.right.update_heights()
            
            self.height = max(self.node.left.height,
                              self.node.right.height) + 1 
        else: 
            self.height = -1 
            
    def update_balances(self, recurse=True):
        if not self.node == None: 
            if recurse: 
                if self.node.left != None: 
                    self.node.left.update_balances()
                if self.node.right != None:
                    self.node.right.update_balances()

            self.balance = self.node.left.height - self.node.right.height 
        else: 
            self.balance = 0 

    def delete(self, key, modo):
        # debug("Trying to delete at node: " + str(self.node.key))
        if self.node != None: 
            if self.node.key == key: 
                debug("Deleting ... " + str(key))  
                if self.node.left.node == None and self.node.right.node == None:
                    self.node = None # leaves can be killed at will 
                # if only one subtree, take that 
                elif self.node.left.node == None: 
                    self.node = self.node.right.node
                elif self.node.right.node == None: 
                    self.node = self.node.left.node
                
                # worst-case: both children present. Find logical successor
                else:  
                    replacement = self.logical_successor(self.node)
                    if replacement != None: # sanity check 
                        debug("Found replacement for " + str(key) + " -> " + str(replacement.key))  
                        self.node.key = replacement.key 
                        
                        # replaced. Now delete the key from right child 
                        self.node.right.delete(replacement.key, modo)
                    
                self.rebalance()
                return  
            else:
                if modo == 0:
                    if key < self.node.key: 
                        self.node.left.delete(key, modo)  
                    else:
                        self.node.right.delete(key, modo)
                else:
                    if key.getSigla() < self.node.key.getSigla() : 
                        self.node.left.delete(key, modo)  
                    else:
                        self.node.right.delete(key, modo)                    
                    
                        
            self.rebalance()
        else: 
            return 

    def logical_predecessor(self, node):
        ''' 
        Find the biggest valued node in LEFT child
        ''' 
        node = node.left.node 
        if node != None: 
            while node.right != None:
                if node.right.node == None: 
                    return node 
                else: 
                    node = node.right.node  
        return node 
    
    def logical_successor(self, node):
        ''' 
        Find the smallese valued node in RIGHT child
        ''' 
        node = node.right.node  
        if node != None: # just a sanity check  
            
            while node.left != None:
                debug("LS: traversing: " + str(node.key))
                if node.left.node == None: 
                    return node 
                else: 
                    node = node.left.node  
        return node 

    def check_balanced(self):
        if self == None or self.node == None: 
            return True
        
        # We always need to make sure we are balanced 
        self.update_heights()
        self.update_balances()
        return ((abs(self.balance) < 2) and self.node.left.check_balanced() and self.node.right.check_balanced())  
        
    def inorder_traverse(self):
        if self.node == None:
            return [] 
        
        inlist = [] 
        l = self.node.left.inorder_traverse()
        for i in l: 
            inlist.append(i) 

        inlist.append(self.node.key)

        l = self.node.right.inorder_traverse()
        for i in l: 
            inlist.append(i) 
    
        return inlist 

    def display(self):
        '''
        Display the whole tree. Uses recursive def.
        TODO: create a better display using breadth-first search
        '''        
        self.update_heights()  # Must update heights before balances 
        self.update_balances()
        if(self.node != None): 
            print (self.node.getData())
            if self.node.left != None: 
                self.node.left.display()
            if self.node.left != None:
                self.node.right.display()
