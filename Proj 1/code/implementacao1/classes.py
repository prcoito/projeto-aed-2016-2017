﻿class Node:
    def __init__(self, init_data):
        self.data = init_data
        self.next = None
        
    def getData(self):
        return self.data
    
    def getNext(self):
        return self.next
    
    def setData(self, new_data):
        self.data = new_data
        
    def setNext(self, new_next):
        self.next = new_next   
        
class LinkedList:
    def __init__(self):
        self.head = None
        
    def getData(self):
        return self.head
    
    def is_empty(self):
        return self.head == None
    
    def add(self, item):
        current = self.head
        previous = None
        stop = False
        if(item.getClassName() == 'Pais'):
            while current != None:# insere o novo pais no fim da lista
                previous = current
                current = current.getNext()
        
            temp = Node(item)
            if previous == None:
                temp.setNext(self.head)
                self.head = temp
            else:
                temp.setNext(current)
                previous.setNext(temp)            
        else:
            while current != None and not stop: # insere informacao ordenada de acordo com o ano
                if current.getData().getAno() > item.getAno():
                    stop = True
                else:
                    previous = current
                    current = current.getNext()
        
            temp = Node(item)
            if previous == None:
                temp.setNext(self.head)
                self.head = temp
            else:
                temp.setNext(current)
                previous.setNext(temp)
                
    def remove(self, item):
        current = self.head
        previous = None
        found = False
        while not found:
            if current.getData() == item:
                found = True
            else:
                previous = current
                current = current.getNext()
        
        if previous == None:
            self.head = current.getNext()
        else:
            previous.setNext(current.getNext())  

    def print_list(self):
        currentNode = self.head
       
        while currentNode != None:
            print(currentNode.getData())
            currentNode = currentNode.getNext()
            
        
    def size(self):
        currentNode = self.head
        l = 0
              
        while currentNode != None:
            l = l+1
            currentNode = currentNode.getNext()
        return l
    
    def search(self, item):
        currentNode = self.head
        if(item.getClassName() == 'Pais'):
            while currentNode!=None:
                if(currentNode.getData().getNome() == item.getNome()):
                    return True
                else:
                    currentNode = currentNode.getNext()
            return False
        else:
            while currentNode!=None:
                if(currentNode.getData().getAno() == item.getAno()):
                    return True, currentNode
                else:
                    currentNode = currentNode.getNext()
            return False, None           
            
    
class Pais:
    def __init__(self, nome, sigla):
        self.nome = nome
        self.sigla = sigla
        self.listaDados = LinkedList()
        
    def addDados(self, ano, percentagem):
        self.listaDados.add(Dados(ano, percentagem))
        return
    
    def removeDados(self, ano, percentagem):
        self.listaDados(remove(Dados(ano,percentagem)))
        return
    
    def inserirDados(self, ano, percentagem):
        boolean, node = self.getListaDados().search(Dados(ano, percentagem)) # verifica se info para aquele ano ja exite
        
        if boolean == False:    # se nao exister adiciona
            self.addDados(ano, percentagem)
        else:                   # se nao é perguntado ao utilizador se quer substituir a informacao
            invalido = True
            print('Erro em',self.getNome())
            op = str(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
            while invalido:
                if op == '1':
                    invalido = False
                    node.getData().editarPercentagem(percentagem)   # se sim entao edita-se a percentagem
                    return
                elif op == '2':
                    invalido = False
                    return
                else:
                    print('Inseriu uma opção inválida')
                op = str(input('Já existe informacao para esse ano. Deseja substituir?\n1-Sim\n2-Não\n'))
        return
    
    def getNome(self):
        return self.nome
    
    def getSigla(self):
        return self.sigla
    
    def getListaDados(self):
        return self.listaDados
    
    def getClassName(self):
        return "Pais"
    
    def __str__(self):
        return self.getNome()+" "+self.getSigla() 
    
    def __eq__(self, other):
        return self.getNome() == other.getNome() #and self.getSigla() == other.getSigla()

class Dados:
    def __init__(self, ano, percentagem):
        self.ano = ano
        self.percentagem = percentagem
        
    def getAno(self):
        return self.ano
    
    def getPercentagem(self):
        return self.percentagem
    
    def editarPercentagem(self, nova):
        self.percentagem = nova
    
    def getClassName(self):
        return "Dados"
    
    def __str__(self):
        return "Ano: " + str(self.getAno()) + " Percentagem: " + str(self.getPercentagem())       
    
    def __eq__(self, other):
        return self.getAno() == other.getAno() and self.getPercentagem() == other.getPercentagem()
    
    def __ne__(self, other):
        return not self.__eq__
    