﻿from classes import *
import csv
#import os
#import psutil
import time

def loadFile():
    #inicio = time.clock()
    with open('../dados.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        row = next(reader) #le primeira linha
        
        arrayPais = []
        arraySigla = []
        
        for i in range(26): # 26 = numero de letras diferentes no alfabeto
            arraySigla.append(LinkedList())
            arrayPais.append(LinkedList())
        
        ano_min = int(row[2])
        ano_max = int(row[len(row)-1])
        
        for row in reader:
            nome = row[0]
            sigla = row[1]
            novoPais = Pais(nome, sigla)
            
            for i in range(2, len(row)):
                if(row[i]!=''):
                    novoPais.addDados((i-2+ano_min), row[i])       #adiciona a lista de dados do pais o valor da percentagem para aquele ano
            
            arraySigla[ord(sigla[0])-65].add(novoPais) # Se sigla pais comeca letra A, vai para a lista de paises na pos 0 do array
                                                 # Se sigla pais começa letra B, vai para a lista de paises na pos 1 do array, e assim sucessivamente
            arrayPais[ord(nome[0])-65].add(novoPais)# Se nome pais começa letra A, vai para a lista de paises na pos 0 do array
                                                 # Se nome pais começa letra B, vai para a lista de paises na pos 1 do array, e assim sucessivamente
        #print(time.clock()*1000-inicio*1000)                                         
        return arraySigla, arrayPais, [ano_min, ano_max]
        
def updateFile(array, ano_min_max):
    #inicio = time.clock()
    
    ano_max = ano_min_max[1]
    ano_min = ano_min_max[0]
    
    with open('../dados.csv', "w", newline='') as csvfile:
        row = []
        for i in range(2+ano_max-ano_min+1):
            row.append('')
            
        writer = csv.writer(csvfile, delimiter=';' ,quotechar='', quoting=csv.QUOTE_NONE)
        row[0] = '"Country Name"'
        row[1] = '"Country Code"'

        for i in range(2, ano_max-ano_min+3):
            row[i] = '"'+str(ano_min-2+i)+'"'
        
        writer.writerow(row)
        
        for k in range(len(array)):
            
            ListaPais = array[k]
           
            nodePais = ListaPais.getData()
                 
            while nodePais != None:
                index = 1
                pais = nodePais.getData()
                
                row[0] = '"'+pais.getNome()+'"'
                row[1] = '"'+pais.getSigla()+'"'
            
                ListaDados = pais.getListaDados()
                nodeDados = ListaDados.getData()
                while nodeDados!=None:
                    dados = nodeDados.getData()
                    ano = dados.getAno()
                    for i in range(index+1, ano-ano_min+2): #ate ao ano nao ha informacao logo row[i] =''
                        row[i] = ''
                    index = ano-ano_min+2
                    row[index] = '"'+str(dados.getPercentagem())+'"'#adiciona-se a percentagem a row
                    nodeDados = nodeDados.getNext() #continua até não haver mais dados a guardar
                    
                for i in range(index+1, ano_max-ano_min+3): # ate ao ano_max nao ha mais informacao logo row[i] =''
                    row[i] = ''                   

                writer.writerow(row)    #escreve a linha no ficheiro e continua ate nao haver mais paises
                nodePais = nodePais.getNext()
                
    #print(time.clock()*1000 - inicio*1000)
    return

def inserirDadosPais(array, string, ano_min_max):    #string é uma palavra em letras maiusculas (nome de pais ou sigla de pais)
    #inicio = time.clock()
    Linkedlist = array[ord(string[0])-65]   # vai buscar a lista onde string está
    node = Linkedlist.getData()
    
    while node!=None and node.getData().getNome().upper()!=string and node.getData().getSigla().upper()!=string:
        node = node.getNext()
    
    if node == None:
        print('Esse pais não existe. Nao foi possivel realizar a operação pretendida')
        return
    #parc = time.clock() - inicio
    ano = int(input('Insira o ano para o qual quer inserir informacao: '))
    if ano > ano_min_max[1] or ano < ano_min_max[0]: #ano_min_max == [ano_min, ano_max]
        print('Esse ano ainda não existe. Utilize a opcao 2 do menu anterior para criar esse ano')
        return
    #inicio = time.clock()
    invalido = True
    while invalido:
        #parc = parc + (time.clock() - inicio)
        percentagem = float(input('Insira a percentagem: '))
        if percentagem>=0 and percentagem<=100:
            invalido = False
        else:
            print('\nInseriu uma percentagem invalida')    
    #inicio = time.clock()
    node.getData().inserirDados(ano, percentagem) #insere info a esse pais
    
    #print(time.clock()*1000 - inicio*1000 + parc*1000)
    return
    
def inserirInfoAnoPais(arraySigla, arrayPais, ano_min_max):
    op = str(input('Insira:\n1-Para escrever sigla do pais.\n2-Para escrever nome do pais.\n3-Para sair\n'))
    while(op!='3'):
        if(op == '1'):
            sigla = str(input('Insira a sigla: '))
            sigla = sigla.upper()
            if not sigla[0] >='A' and sigla[0] <= 'Z':
                print("País inválido") 
            else:
                inserirDadosPais(arraySigla, sigla, ano_min_max)
        elif(op == '2'):
            nome = str(input('Insira nome do pais: '))
            nome = nome.upper()
            if not nome[0] >='A' and nome[0] <= 'Z':
                print("País inválido") 
            else:
                inserirDadosPais(arrayPais, nome, ano_min_max)
        elif(op == '3'):
            break
        else:
            print('Inseriu uma opcao invalida\n')
        op = str(input('Insira:\n1-Para escrever sigla do pais.\n2-Para escrever nome do pais.\n3-Regressar ao menu anterior\n'))
        
    return

def inserirAnoTodosPaises(arraySigla, arrayPais, ano_min_max):
    #inicio = time.clock()
    ano = int(input('Insira o ano para o qual quer inserir informacao: '))
    if ano >= ano_min_max[0] and ano <= ano_min_max[1]:
        print('O ano inserido já exite')
    else:
        print('A percentagem para esse ano, em todos os paises, será 0 (zero).\nEsta pode ser mais tarde alterada')
        for i in arraySigla:    #percorre todas as listas ligadas
            nodePais = i.getData()
            while nodePais != None: 
                nodePais.getData().addDados(ano, 0) # adiciona info ao pais
                nodePais = nodePais.getNext() 
        
        # atualizacao de ano_min_max
        if ano < ano_min_max[0]:
            ano_min_max[0] = ano
        if ano > ano_min_max[1]:
            ano_min_max[1] = ano
    
    #print(time.clock()*1000 - inicio*1000)           
    return ano_min_max
    
def inserirDadosTodosPaises(arraySigla, arrayPais, ano_min_max):
    
    ano = int(input('Insira o ano para o qual quer inserir informacao: '))
    if ano > ano_min_max[1] or ano < ano_min_max[0]:
        op = int(input('Esse ano ainda não existe. Deseja cria-lo?\n1-Sim\n2-Não\n'))
        invalido = True
        while invalido:
            if op == 1:
                invalido = True
                while invalido:
                    percentagem = float(input('Insira a percentagem: '))
                    if percentagem>=0 and percentagem<=100:
                        invalido = False
                    else:
                        print('\nInseriu uma percentagem invalida')  
                        
                for i in arraySigla:    #percorre todas as listas ligadas
                    nodePais = i.getData()
                    while nodePais != None:
                        nodePais.getData().addDados(ano, percentagem) # adiciona info ao pais
                        nodePais = nodePais.getNext()   
                        
                # atualizacao de ano_min_max        
                if ano < ano_min_max[0]:
                    ano_min_max[0] = ano
                if ano > ano_min_max[1]:
                    ano_min_max[1] = ano
            elif op == 2:
                break
            else:
                print('Inseriu uma opcao invalida')
                op = int(input('Esse ano ainda não existe. Deseja cria-lo?\n\1-Sim\n2-Não\n'))
                    
    else:            
        invalido = True
        while invalido:
            percentagem = float(input('Insira a percentagem: '))
            if percentagem>=0 and percentagem<=100:
                invalido = False
            else:
                print('\nInseriu uma percentagem invalida')  
        #inicio = time.clock()        
        for i in arraySigla:        #percorre todas as listas ligadas
            nodePais = i.getData()
            while nodePais != None:
                nodePais.getData().addDados(ano, percentagem)  # adiciona info ao pais
                nodePais = nodePais.getNext()
        print('Dados inseridos com sucesso')
    #print(time.clock()*1000 - inicio*1000)
    return ano_min_max

def inserirDados(arraySigla, arrayPais, ano_min_max):
    op = str(input('Que operacao pretender realizar?\n1-Inserir informacao de um ano para um pais.\n2-Criar um novo ano(para todos os paises)\n3-Inserir dados para todos os paises\n4-Regressar ao menu principal\n'))
    
    while op!='4':
        if op == '1':
            inserirInfoAnoPais(arraySigla, arrayPais, ano_min_max)
        elif op == '2':
            ano_min_max = inserirAnoTodosPaises(arraySigla, arrayPais, ano_min_max)
        elif op == '3':
            ano_min_max = inserirDadosTodosPaises(arraySigla, arrayPais, ano_min_max)
        elif op == '4':
            break
        else:
            print('Inseriu uma opcao errada')
        op = str(input('Que operacao pretender realizar?\n1-Inserir informacao de um ano para um pais.\n2-Inserir um novo ano(para todos os paises)\n3-Inserir dados para todos os paises\n4-Regressar ao menu principal\n'))
    
    return ano_min_max

def editar(arraySigla, arrayPais, ano_min_max):
    op=str(input("Introduza o número da ação que deseja executar:\n1.Introduzir sigla do pais\n2.Introduzir nome do país\n3.Voltar para o menu anterior\n"))
    while (op != '3'):
        if(op == '1'):
            sigla = str(input('Insira a sigla: '))
            sigla = sigla.upper()
            if not sigla[0] >='A' and sigla[0] <= 'Z':
                print("País inválido") 
            else:
                editarDadosSigla(arraySigla, sigla, ano_min_max)
        elif(op == '2'):
            nome = str(input('Insira nome do pais: '))
            nome = nome.upper()
            if not nome[0] >='A' and nome[0] <= 'Z':
                print("País inválido") 
            else:
                editarDadosPais(arrayPais, nome, ano_min_max)
        else:
            print("Opção invalida")
        op=str(input("Introduza o número da ação que deseja executar:\n1.Introduzir sigla do pais\n2.Introduzir nome do país\n3.Voltar para o menu anterior\n"))
    
    
def editarDadosPais(arrayListas, nome, ano_min_max):
    #inicio = time.clock()
    nodePais = arrayListas[ord(nome[0])-65].getData()
    
    while nodePais!= None:  #procura pais na lista ligada
        if nodePais.getData().getNome().upper() == nome.upper():
            break
        nodePais = nodePais.getNext()
        
    if nodePais == None:
        print('Pais inserido nao existe')
        return
    #parc = time.clock() - inicio
    ano = int(input("Qual o ano que quer editar? "))
    if ano< ano_min_max[0] or ano > ano_min_max[1]:
        print('Ano inserido invalido')
        return
    #inicio = time.clock()
    boolean, dados = nodePais.getData().getListaDados().search(Dados(ano, 0)) # verifica se input é valido
    if boolean == False:    # se nao for mostra erro
        print('Nao existe informacao a editar nesse ano para esse pais')
        return     
    
    #parc +=time.clock()-inicio
    invalido = True
    while invalido:
        percentagem = float(input("Novo Valor de percentagem: "))
        if percentagem>=0 and percentagem <=100:
            invalido = False
        else:
            print('Percentagem inserida invalida')
    
    #inicio = time.clock()
    dados.getData().editarPercentagem(percentagem)  # atualiza percentagem
    #print(time.clock()*1000 - inicio*1000 + parc*1000)
    return

def editarDadosSigla(arrayListas, sigla, ano_min_max):
    
    nodePais = arrayListas[ord(sigla[0])-65].getData()
    
    while nodePais!= None:  #procura pais na lista ligada
        if nodePais.getData().getSigla().upper() == sigla.upper():
            break
        nodePais = nodePais.getNext()
        
    if nodePais == None:
        print('Pais inserido nao existe')
        return
    
    ano = int(input("Qual o ano que quer editar? "))
    if ano< ano_min_max[0] or ano > ano_min_max[1]:
        print('Ano inserido invalido')
        return
    
    boolean, dados = nodePais.getData().getListaDados().search(Dados(ano, 0)) # verifica se input é valido
    if boolean == False:    # se nao for mostra erro
        print('Nao existe informacao a editar nesse ano para esse pais')
        return     
    
    invalido = True
    while invalido:
        percentagem = float(input("Novo Valor de percentagem: "))
        if percentagem>=0 and percentagem <=100:
            invalido = False
        else:
            print('Percentagem inserida invalida')
    
    dados.getData().editarPercentagem(percentagem)  # atualiza percentagem
    return

def menu(arraySigla,arrayPais, ano_min_max):
    n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa\n2.Inserção\n3.Edição\n4.Remoção\n5.Listar países\n6.Sair\n"))
    while (n!='6'):
        if(n=='1'):
            searchMenu(arraySigla,arrayPais, ano_min_max)
        elif(n=='2'):
            ano_min_max = inserirDados(arraySigla, arrayPais, ano_min_max)
        elif(n=='3'):
            editar(arraySigla, arrayPais, ano_min_max)
        elif(n=='4'):
            remover(arraySigla, arrayPais, ano_min_max)
        elif(n=='5'):
            listarP(arrayPais)
        else:
            print("Opção invalida")
        n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa\n2.Inserção\n3.Edição\n4.Remoção\n5.Listar países\n6.Sair\n"))
    return ano_min_max

def searchMenu(arraySigla, arrayPais, ano_min_max):
    n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa por país\n2.Pesquisa por ano\n3.Pesquisar por sigla\n4.Voltar para o menu principal\n"))
    while (n!='4'):
        if(n=='1'):
            searchC(arrayPais)
        elif(n=='2'):
            searchY(arrayPais, ano_min_max)
        elif(n=='3'):
            searchS(arraySigla)
        else:
            print("Opção invalida\n")  
        n=str(input("Introduza o número da ação que deseja executar:\n1.Pesquisa por país\n2.Pesquisa por ano\n3.Pesquisar por sigla\n4.Voltar para o menu principal\n"))
    return
        
def searchC(arrayPais):#imprime todos os dados de um pais
    nome=str(input("Introduz o nome do país que pretende pesquisar: "))
    
    #inicio = time.clock()
    if not nome[0].upper() >='A' and nome[0].upper() <= 'Z':
        print("País inválido") 
        return
    
    
    cLetter = arrayPais[ord(nome[0].upper())-65].getData()

    while cLetter!=None: #caso se chegue ao fim
        if cLetter.getData().getNome().lower()==nome.lower():
            datalist=cLetter.getData().getListaDados()
            if datalist.size() == 0:
                print('Nao existem dados para esse pais')
            else:
                datalist.print_list()
            #print(time.clock()*1000 - inicio*1000)    
            return
        cLetter=cLetter.getNext() #avança para outro no (pais)        
    
    print('Pais nao existente')
    return
        
def searchY(arrayPais, ano_min_max): #imprime info de todos os paises num deperminado ano
    n=int(input("Introduza o ano que quer pesquisar: "))

    #inicio = time.clock()
    if n>ano_min_max[1] or n<ano_min_max[0]:
        print('Ano inserido invalido')
        return
    
    for i in arrayPais:
        npais=i.getData()
        while (npais!=None):
            dado=npais.getData().getListaDados().getData()
            while (dado!=None):
                if (dado.getData().getAno()==n):
                    print("País: " + npais.getData().getNome() + "--------Percentagem no ano pedido: " + str(dado.getData().getPercentagem()))
                    break
                elif dado.getData().getAno()>n:
                    break # como o ano do nó é maior do que o inserido, e como está ordenado por anos na lista ligada podemos parar de verificar se o ano está na lista ligada
                dado=dado.getNext()
            npais=npais.getNext()
            
    #print(time.clock()*1000 - inicio*1000)  
    return

def searchS(arraySigla):
    sigla=str(input("Introduz a sigla do país que pretende pesquisar: "))
   
    #inicio = time.clock()
    if not sigla[0].upper() >='A' and sigla[0].upper() <= 'Z':
        print("País inválido") 
        return
    
    sLetter = arraySigla[ord(sigla[0].upper())-65].getData()
    while (sLetter!=None): #caso se chegue ao fim
        if(sLetter.getData().getSigla().lower()==sigla.lower()):
            datalist=sLetter.getData().getListaDados()
            if datalist.size() == 0:
                print('Nao existem dados para esse pais')
            else:
                datalist.print_list()
            #print(time.clock()*1000 - inicio*1000)
            return
        sLetter=sLetter.getNext() #avança para outro no (pais)        
    
    
    print('Pais nao existente')
    return


def listarP(arrayPais):
    #inicio = time.clock()
    for listp in arrayPais:
        listp.print_list()
    #print(time.clock()*1000 - inicio*1000)

def remover(arraySigla, arrayPais, ano_min_max):
    n=str(input("Introduza o número da ação que deseja executar:\n1.Remover uma percentagem (introduzir nome do país)\n2.Remover uma percentagem (introduzir sigla do pais)\n3.Remover percentagens de um ano\n4.Remover percentagens de um pais\n5.Voltar para o menu anterior\n"))
    while (n!='5'):
        if(n=='1'):
            removerPais(arrayPais,ano_min_max)
        elif(n=='2'):
            removerSigla(arraySigla, ano_min_max)
        elif(n == '3'):
            removerAno(arrayPais,ano_min_max)
        elif(n=='4'):
            removerPaisTodo(arrayPais,ano_min_max)
        else:
            n=str(input("Introduza uma opção valida"))
        n=str(input("Introduza o número da ação que deseja executar:\n1.Remover uma percentagem (introduzir nome do país)\n2.Remover uma percentagem (introduzir sigla do pais\n3.Remover percentagens de um ano\n4.Remover percentagens de um pais\n5.Voltar para o menu anterior\n"))
    return

def removerPais(arrayPais, ano_min_max):
    nome = str(input('Insira o nome do pais: '))
    
    if not nome[0].upper() >='A' and nome[0].upper() <= 'Z':
        print("País inválido") 
        return
        
    cLetter = arrayPais[ord(nome[0].upper())-65].getData()
    
    while(cLetter!= None):
        if(cLetter.getData().getNome().lower() == nome.lower()):
            ano = int(input("Pais existe. Qual o ano que quer remover? "))
            if ano< ano_min_max[0] or ano > ano_min_max[1]:
                print('Ano inserido invalido')
                return
            
            boolean, nodeDados = cLetter.getData().getListaDados().search(Dados(ano, 0)) # verifica se ha info a remover
    
            if boolean == False:
                print('Esse ano já está sem informação.')
                return
            else:
                cLetter.getData().getListaDados().remove(nodeDados.getData())
                print('Informação removida com sucesso')
                return
    
        cLetter = cLetter.getNext()
        
    if cLetter == None:
        print('Pais inserido nao existe')
        return
    
    return

def removerSigla(arraySigla, ano_min_max):
    sigla = str(input('Insira a sigla do pais: '))
    
    if not sigla[0].upper() >='A' and sigla[0].upper() <= 'Z':
        print("País inválido") 
        return
        
    cLetter = arraySigla[ord(sigla[0].upper())-65].getData()
    
    while(cLetter!= None):
        if(cLetter.getData().getSigla().lower() == sigla.lower()):
            ano = int(input("Pais existe. Qual o ano que quer remover? "))
            if ano< ano_min_max[0] or ano > ano_min_max[1]:
                print('Ano inserido invalido')
                return
            
            boolean, nodeDados = cLetter.getData().getListaDados().search(Dados(ano, 0)) # verifica se ha info a remover
            if boolean == False:
                print('Esse ano já está sem informação.')
                return
            else:
                cLetter.getData().getListaDados().remove(nodeDados.getData())
                print('Informação removida com sucesso')
                return
        cLetter = cLetter.getNext()
        
    if cLetter == None:
        print('Pais inserido nao existe')
        return
    
    return

def removerAno(arrayPais,ano_min_max):
    ano = int(input("Ano a remover: "))
    
    if(ano < ano_min_max[0] or ano > ano_min_max[1]):
        print('Ano inserido invalido')
        return
    
    #print("Ano válido!")
    
    for i in range (26): # percorre todas as listas ligadas
        cLetter = arrayPais[i].getData()
        
        while(cLetter != None):
            boolean, nodeDados = cLetter.getData().getListaDados().search(Dados(ano, 0)) # verifica se ha info a remover
            if boolean == True:
                cLetter.getData().getListaDados().remove(nodeDados.getData())
                #print('Informação removida com sucesso')
            cLetter = cLetter.getNext()
    
    print('Remoção concluida')
    return
            
def removerPaisTodo(arrayPais,ano_min_max):
    nome = str(input('Insira o nome do pais: '))
    
    if not nome[0].upper() >='A' and nome[0].upper() <= 'Z':
        print("País inválido") 
        return
        
    cLetter = arrayPais[ord(nome[0].upper())-65].getData()
    
    while(cLetter!= None):
        if(cLetter.getData().getNome().lower() == nome.lower()):
            print("Pais existe. A remover...")
            cLetterNode = cLetter.getData().getListaDados().getData()
            while(cLetterNode != None):
                nodeDados = cLetter.getData().getListaDados().getData()
                cLetter.getData().getListaDados().remove(nodeDados.getData())
                cLetterNode = cLetterNode.getNext()
        cLetter = cLetter.getNext()
    
    print("Remoção terminada!")
    return

def main():
    
    arraySigla, arrayPais, ano_min_max = loadFile()
    
    #process = psutil.Process(os.getpid())
    #print(process.memory_info().rss/1024/1024)    
    
    ano_min_max = menu(arraySigla, arrayPais, ano_min_max)

    updateFile(arraySigla, ano_min_max)
    
if __name__ == "__main__":
    
    main()