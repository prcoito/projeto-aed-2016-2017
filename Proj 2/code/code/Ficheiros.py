﻿import os
#import random
#import time
import re
from Algoritmos import *
#from Geradores import *
from Graph import *
#from PrimSpanningTree import *
#from PriorityQueue import *


def parseFile(f):
    line = f.readline()
    Nline = line.strip("O Grafo tem ")
    if Nline == line:
        return None
    line = Nline
    numVertices = int(re.search(r'\d+', line).group()) #encontra 1o numero em line

    Nline = line.replace(str(numVertices)+" vertices [","")
    if Nline == line:
        return None
    line = Nline    
    
    Nline = line.strip("]\n")
    if Nline == line:
        return None
    line = Nline 
    
    cities = line.split(',')
    cities = [x.replace(" ","") for x in cities] #assegura que nomes nao tem espaco antes

    G = Graph()
    for i in range(numVertices):
        G.addVertex(cities[i])
        
    for i in range(numVertices):
        v1 = cities[i]
        line = f.readline()
        line = line.replace("Vertice "+str(v1)+" connectado com:\n","")
        if line!="":
            return None
        
        for j in range(numVertices-1):
            line = f.readline()
            Nline = line.replace("\tVertice ","")
            if Nline == line:
                return None
            line = Nline
            v2 = line.split(" ",1)[0]
            Nline = line.replace(str(v2)+" [distancia = ", "")
            if Nline == line:
                return None 
            line = Nline
            Nline = line.replace("]\n","")
            if Nline == line:
                return None 
            line = Nline
            d = int(line)
            G.addEdge(v1,v2,d)
        f.readline()  #le linha em branco
    return G


def readGraphFromFile():
    os.system('cls' if os.name == 'nt' else 'clear') 
    G = None
    dirFiles=[]
    path = os.getcwd()+"\Ficheiros txt\\"
    for file in os.listdir(os.getcwd()+"\Ficheiros txt"):
        if file.endswith(".txt"):
            dirFiles+=[file]  
    
    if len(dirFiles) == 0:
        print("ERRO!!!\nNão existem grafo guardados na pasta Ficheiros txt")
    else:
        string = ""
        for i in range(len(dirFiles)):
            string+="\t"+str(i+1)+" - "+dirFiles[i]+";\n"
            
        erro = True
        while erro:
            op = str(input("Insira qual o ficheiro que pretende abrir\n"+string+"Opcao: "))
            if isInt(op):
                if op>='1' and int(op)<=len(dirFiles):
                    erro = False
                    f = open(path+dirFiles[int(op)-1])
                    G = parseFile(f)
                    f.close()
                    if G == None:
                        print("ERRO!!!\nO ficheiro escolhido está corrupto.")
                    else:
                        print("Ficheiro carregado com sucesso")
                else:
                    print("ERRO!!!\nInseriu uma opcao invalida\n")                    
            else:
                print("ERRO!!!\nInseriu uma opcao invalida\n")
                
            
    input("Prima qualquer tecla para continuar\n") 
    os.system('cls' if os.name == 'nt' else 'clear') 
    return G

def saveGraphToFile(G):
    os.system('cls' if os.name == 'nt' else 'clear') 
    path = os.getcwd()+"\Ficheiros txt\\"
    filename=str(input("Insira o nome do ficheiro em que quer guardar o grafo: "))
    filename+=".txt"
    f = open(path+filename, "w")
    x = []
    for i in G:
        x+=[i.getId()] 
    f.write("O Grafo tem "+str(G.getNumVertices())+" vertices "+str(x)+"\n")
    for i in G:
        f.write("Vertice "+str(i.getId())+" connectado com:\n")
        for k in i.connectedTo:
            f.write("\tVertice "+str(k.id)+" [distancia = "+str(i.getWeight(k))+"]\n")
        f.write("\n")
    f.close()
    
    input("Ficheiro gravado com sucesso!\nPrima qualquer tecla para continuar\n") 
    os.system('cls' if os.name == 'nt' else 'clear')     
    return

def saveGraphToFileO(G):
    os.system('cls' if os.name == 'nt' else 'clear') 
    path = os.getcwd()+"\Ficheiros txt\\"
    filename=str(input("Insira o nome do ficheiro em que quer guardar o grafo: "))
    filename+=".txt"
    f = open(path+filename, "w")
    #x = []
    #for i in G:
        #x+=[i.getId()] 
    f.write("O Grafo tem "+str(G.getNumVertices())+"\n")
    for i in G:
        f.write("V"+str(i.getId())+"com:\n\t")
        for k in i.connectedTo:
            f.write("V"+str(k.id)+" d="+str(i.getWeight(k))+",")
        f.write("\n")
    f.close()
    
    input("Ficheiro gravado com sucesso!\nPrima qualquer tecla para continuar\n") 
    os.system('cls' if os.name == 'nt' else 'clear')     
    return
