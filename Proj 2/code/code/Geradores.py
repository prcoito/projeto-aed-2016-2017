﻿import random
import os
#import time
#import re
#from Algoritmos import *
#from Ficheiros import *
from Graph import *
#from PrimSpanningTree import *
#from PriorityQueue import *


#Gera grafo completo com n vertices em que distancia de A para B é igual à distancia de B para A
def geraGrafoCompleto1(n):
    g = Graph()
    for i in range(n):  #cria n vertices no grafo
        g.addVertex(i+1)
        
    for i in g.vertList:
        for k in g.vertList:
            if i!=k:
                d = random.randint(10, 50) #d entre 10 e 50 km
                g.addEdge(i, k, d)
                g.addEdge(k, i, d)
    
    #print(g)
    #print(g.isComplete())
                
    print('Grafo gerado com sucesso.\nPode ver o grafo gerado escolhendo a opcao "Imprimir Grafo" no menu principal')
    input("Prima qualquer tecla para continuar\n") 
    os.system('cls' if os.name == 'nt' else 'clear')     
    return g        

#Gera graafo completo com n vertices em que distancia de A para B pode não ser igual à distancia de B para A
def geraGrafoCompleto2(n):
    g = Graph()
    for i in range(n):  #cria n vertices no grafo
        g.addVertex(i+1)
        
    for i in g.vertList:
        for k in g.vertList:
            if i!=k:
                d = random.randint(10, 50) #d entre 10 e 50 km
                g.addEdge(i, k, d)
    
    #print(g)
    #print(g.isComplete())
                
                
    print('Grafo gerado com sucesso.\nPode ver o grafo gerado escolhendo a opcao "Imprimir Grafo" no menu principal')
    input("Prima qualquer tecla para continuar\n") 
    os.system('cls' if os.name == 'nt' else 'clear')   
    return g           
                

def geraVerticeInicial(g):
    i = random.randint(1,g.getNumVertices())
    v = g.getVertex(i)
    v.init = True
    return v