﻿#import random
#import time
#import re
#from Algoritmos import *
#from Ficheiros import *
#from Geradores import *
#from PrimSpanningTree import *
#from PriorityQueue import *


class Vertex:
    def __init__(self,key):
        self.id = key
        self.connectedTo = {}
        self.dist = float("inf")    # usado spanning tree
        self.pred = None            # usado spanning tree
        self.discovered = False     # usado em Depth-first search

    def addNeighbor(self,nbr,weight=0):
        self.connectedTo[nbr] = weight
    
    def spanningTree(self):#devolve info do vertice referente à spanning tree   (debug)
        string = str(self.id) + "-> dist " + str(self.dist) + "\npred ["
        if self.pred !=None:
            string+=str(self.pred.id)
        string += "]\n"
        return string
        
     
    def __str__(self):
        output="\tVertice "+str(self.id)+" connectedado a:\n"
        for i in self.connectedTo:
            output+="\t\tVertice "+str(i.id)+" [distancia = "+str(self.connectedTo[i])+"km's]\n" 
        return str(output)
        #return str([x.id for x in self.connectedTo])
    
    def __contains__(self, n):
        for x in self.connectedTo:
            if(x.id == n):
                return True
        return False
    
    def getConnections(self):
        return self.connectedTo.keys()

    def getId(self):
        return self.id

    def getWeight(self,nbr):
        if nbr == None:
            return float('inf')
        return self.connectedTo[nbr]
    
    def setDistance(self, d):
        self.dist = d
    
    def setPred(self,p):
        self.pred = p
        
    def getDistance(self):
        return self.dist    
        
    def getNearest1(self, toPass):#devolve vertice mais proximo em toPass
        nearest = None
        for i in self.connectedTo:
            if (self.getWeight(i)<self.getWeight(nearest)):
                if(i in toPass):
                    nearest = i  
        return nearest
    
    def getNearest2(self, toPass): #devolve vertice mais proximo em toPass + optimizado
        nearest = None
        distance = float("inf")
        for i in toPass:
            tempDistance = self.getWeight(i)
            #if (self.getWeight(i)<self.getWeight(nearest)):
            if tempDistance < distance:
                nearest = i
                distance = tempDistance
        return nearest, distance    
    
class Graph:
    def __init__(self):
        self.vertList = {}
        self.numVertices = 0
    
    def getNumVertices(self):
        return self.numVertices
        
    def addVertex(self,key):
        self.numVertices = self.numVertices + 1
        newVertex = Vertex(key)
        self.vertList[key] = newVertex
        return newVertex

    def getVertex(self,n):  # n = Vertex.id
        for i in self:
            if str(i.id) == str(n):
                return i
        else:
            return None

    def __contains__(self,n):
        return n in self.vertList
    
    def __str__(self):
        output = "Grafo com "+str(self.numVertices)+" vertices:\n"
        for i in self:
            output+=str(i)+"\n"
        return output

    def addEdge(self,f,t,cost=0):
        if f not in self.vertList:
            nv = self.addVertex(f)
        if t not in self.vertList:
            nv = self.addVertex(t)
        self.vertList[f].addNeighbor(self.vertList[t], cost)

    def getVertices(self):
        return self.vertList.keys()

    def __iter__(self):
        return iter(self.vertList.values())
    
    def getOriginVertices(self):
        #percorrer todos os vertices
        # verificar se vertice conecta com outra mas outro nao conecta com vertice(aresta unidirecional)
        OriginVertices=[]
        for k in self.vertList:
            flag = True
            for k2 in self.vertList:
                if k!=k2 and flag:
                    v = self.getVertex(k2)
                    if k in v:
                        flag = False
                        
            if flag:
                OriginVertices+=[k]        
        return OriginVertices
    
    def getSinkVertices(self):
        SinkVertices=[]
        for k in self.vertList:
            v = self.getVertex(k)
            if len(v.getConnections())==0:
                SinkVertices+=[k]
        return SinkVertices
    
    def getIsolatedVertices(self):
        IsolatedVertices=[]
        for k in self.vertList:
            v = self.getVertex(k)
            if len(v.getConnections())==0:
                flag = True
                for i in self.vertList:
                    if i!=k and flag:
                        v2 = self.getVertex(i)
                        if k in v2:
                            flag = False
                if flag:
                    IsolatedVertices+=[k]            
        return IsolatedVertices
    
    def isComplete(self):
        for k in self.vertList:
            for i in self.vertList:
                if k!=i:
                    if k not in self.getVertex(i):
                        return False
        return True
    
    def spanningTree(self):#imprime info da spanning tree do grafo      (debug)
        string = ""
        for i in self:
            string+=i.spanningTree()+"\n"
        return string    
    