﻿import os
#import random
import time
#import re
#from Ficheiros import *
from Geradores import *
from Graph import *
from PrimSpanningTree import *
#from PriorityQueue import *

from itertools import permutations

class Queue:
    def __init__(self):
        self.items = []
    def is_empty(self):
        return self.items == []
    def enqueue(self, item):
        self.items.insert(0,item)
    def dequeue(self):
        return self.items.pop()
    def size(self):
        return len(self.items)

def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
    
    #Nearest neighbour algorithm
def NearestNeighbour1(G, start):
    os.system('cls' if os.name == 'nt' else 'clear') 
    
    must_visit=[]
    for i in range(1,G.numVertices+1):
        if(G.getVertex(i)!=start):
            must_visit +=[G.getVertex(i)]
                
    path = [start]
    while must_visit:
        nearest = path[-1].getNearest1(must_visit)
        path.append(nearest)
        must_visit.remove(nearest)
                    
    path+=[start]
    string = str(start.id)
    for i in range(1,len(path)):
        string +="->"+str(path[i].id)
    
    dist = 0
    for i in range(len(path)-1):
        dist+=path[i].getWeight(path[i+1])
        
    print("Caminho minimo: ",string, "\nDistancia total = ",dist,"km's")
    input("Prima qualquer tecla para continuar\n")
    os.system('cls' if os.name == 'nt' else 'clear') 
    return
    #return string, dist
        
    #Nearest neighbour algorithm + optimizado
def NearestNeighbour2(G, start):
    os.system('cls' if os.name == 'nt' else 'clear') 
    
    must_visit = list(G.vertList.values())
    must_visit.remove(start)
    #print(must_visit)
    
    path = [start]
    string = str(start.id)
    distance = 0
    while must_visit:
        nearest, d = path[-1].getNearest2(must_visit)
        distance +=d
        path.append(nearest)
        string +="->"+str(nearest.id)
        must_visit.remove(nearest)
                    
    path+=[start]
    distance += path[-2].getWeight(path[-1])
    string += "->"+str(start.id)
    
    
    print("Caminho minimo: ",string, "\nDistancia total = ",distance,"km's")
    input("Prima qualquer tecla para continuar\n")
    os.system('cls' if os.name == 'nt' else 'clear') 
    return    
    #return string, distance    
            
    #algoritmo baseado em 
    #  http://www.geeksforgeeks.org/travelling-salesman-problem-set-2-approximate-using-mst/
    #  http://www.personal.kent.edu/~rmuhamma/Algorithms/MyAlgorithms/AproxAlgor/TSP/tsp.htm
    #  http://www.cs.uni.edu/~fienup/cs270s04/lectures/lec29_4-27-04.htm

def getSpanningTreePathDistance(g, vertex):
    os.system('cls' if os.name == 'nt' else 'clear') 
    #Constroi-se a spanning tree do grafo g
    #Constroi-se um novo grafo que é a spanning tree do grafo g
    #Obtem-se a preorder walk atraves do algoritmo Depth-first search
    
    PrimsSpanningTree(g, vertex)
    # constroi um novo grafo a partir da spanning tree de g
    sT = Graph()
    for i in g:
        sT.addVertex(i.id)
        
    for i in g:
        pred = i.pred
        if pred!=None:
            d = i.dist
            v = sT.getVertex(pred.id)
            v2 = sT.getVertex(i.id)
            v.addNeighbor(v2,d)
                
            
    initST = sT.getVertex(vertex.id)
        
    path = DFS(sT,initST) #obter preorder walk
    #path = BFS(sT,initST)
    path+=[initST]
    string = str(path[0].id)
    for i in range(1,len(path)):
        string +="->"+str(path[i].id)
        
    distance = 0
    for i in range(len(path)-1):
        distance+=g.getVertex(path[i].id).getWeight(g.getVertex(path[i+1].id)) # para ir buscar vertices originais
                                                                               # nos quais estao as ligacoes de todos os vertices enquanto que em sT apenas estao as ligacoes da spanning tree
    
    print("Caminho minimo: ",string, "\nDistancia total = ",distance,"km's")
    input("Prima qualquer tecla para continuar\n")
    os.system('cls' if os.name == 'nt' else 'clear') 
    return        
    #return string, distance


def getSpanningTreePathDistanceO(g, vertex):
    
    os.system('cls' if os.name == 'nt' else 'clear') 
    #Constroi-se a spanning tree do grafo g
    #Constroi-se um novo grafo que é a spanning tree do grafo g
    #Obtem-se a preorder walk atraves do algoritmo Depth-first search
    
    PrimsSpanningTree(g, vertex)
    # constroi um novo grafo a partir da spanning tree de g
    sT = Graph()
    for i in g:
        sT.addVertex(i.id)
        
    for i in g:
        pred = i.pred
        if pred!=None:
            d = i.dist
            v = sT.getVertex(pred.id)
            v2 = sT.getVertex(i.id)
            v.addNeighbor(v2,d)
                
            
    initST = sT.getVertex(vertex.id)
        
    string,distance = DFSO(g,initST) #obter preorder walk em que retorna tambem a distancia percorrida
    #path = BFS(sT,initST)
    #path+=[initST]
    #string = str(path[0])
    #for i in range(1,len(path)):
        #string +="->"+str(path[i])
        
    print("Caminho minimo: ",string, "\nDistancia total = ",distance,"km's")
    input("Prima qualquer tecla para continuar\n")
    os.system('cls' if os.name == 'nt' else 'clear') 
    return        

  
def DFS(G,v):   #Depth-first search
    S =[]
    path=[]
    S.append(v)
    while S:
        v = S.pop()
        if not v.discovered:
            v.discovered = True
            path+=[v]
            for i in v.connectedTo:
                S.append(i)
    return path

    
def DFSO(G,initial):   #Depth-first search
    S =[]
    path=[]
    distance = 0
    
    initial.discovered = True
    path+=[initial.id]
    for i in initial.connectedTo:
        S.append(i)    
    
    string = str(path[0])    
    while S:
        v = S.pop()
        if not v.discovered:
            v.discovered = True
            distance+=G.getVertex(path[-1]).getWeight(G.getVertex(v.id))
            path+=[v.id]
            string +="->"+str(v.id)
            for i in v.connectedTo:
                S.append(i)
                
    distance+=G.getVertex(path[-1]).getWeight(G.getVertex(initial.id))
    string +="->"+str(initial.id)
    #path+=[initial.id]
    return string, distance
    
#nao usado
def BFS(G,v): #Breadth-first search
    S = Queue()
    path=[]
    S.enqueue(v)
    while S.size()>0:
        v = S.dequeue()
        if not v.discovered:
            v.discovered = True
            path+=[v]
            for i in v.connectedTo:
                S.enqueue(i)
    return path
        
        
def bruteForce(G, start):
    os.system('cls' if os.name == 'nt' else 'clear') 
    #Finds the shortest route to visit all the cities by bruteforce.
       #Time complexity is O(N!), so never use on long graphs.
    if G.getNumVertices()>=12:
        print("Não é aconcelhavel usar este algoritmo para um grafo com este tamanho.\nO tempo necessario para determinar um caminho e enorme")
        opt = str(input("Deseja continuar? (sim/nao)\n"))
        if (opt.lower() != "sim"):
            os.system('cls' if os.name == 'nt' else 'clear') 
            return
        
    if start is None:
        start = next(iter(G.vertList))
    
    start = start.id
    listVertex = list(G.vertList)
    listVertex.append(start)
    
    distance = float("inf")
    for perm in permutations(listVertex):
        if perm[0] == start and perm[len(perm)-1] == start:
            d = total_distance(G, perm)
            if d < distance:
                distance = d
                path = perm
    
    string = str(path[0])
    for i in range(1,len(path)):
        string +="->"+str(path[i])   
        
    print("Caminho minimo: ",string, "\nDistancia total = ",distance,"km's")
    input("Prima qualquer tecla para continuar\n") 
    os.system('cls' if os.name == 'nt' else 'clear') 
    return
    #return string, dist

def total_distance(G, points):
    #"""
    #Returns the length of the path passing throught
    #all the points in the given order.
    #"""
    d=0
    for i in range(len(points)-1):
        d += G.getVertex(points[i]).getWeight(G.getVertex(points[i+1]))
        
    return d

#usado em testes
#if __name__ == "__main__":
    #k=1
    #while(k<=1):
        #print(k)
        #g = geraGrafoCompleto1(12)
        #initial = g.getVertex(1)
        #t = time.time()
        #bruteForce(g, initial)
        #tf = time.time()
        #print("\tBruteForce")
        #string = str(path[0])
        #for i in range(1,len(path)):
            #string +="->"+str(path[i])        
        #print("\t\tPath", string)        
        #print("\t\tDistance =",bruteDistance,"km's", time.time()-t,"s")
        #print(tf-t)
        
        #print("\tNearestNeighbor")
        #path, distance = getShortestPath1(g, initial) 
        #erro = 100 - (bruteDistance/distance)*100  
        #strErro = " [erro ="+"{0:.2f}".format(erro)+"%]"
        #print("\t\tPath", path)        
        #print("\t\tDistance =",distance,"km's",strErro) 
        
        #print("\tChristofides like algorithm")
        #t = time.time()
        #getSpanningTreePathDistance(g, initial)
        #tf1 = time.time()
        #getSpanningTreePathDistanceO(g, initial)
        #tf2 = time.time()
        #print(tf1-t,"    ",tf2-tf1)
        #distance = 0
                
        #string = str(path[0].id)
        #for i in range(1,len(path)):
            #string +="->"+str(path[i].id)
        #print("\t\tPath", string)
        #for i in range(len(path)-1):
            #v1 = g.getVertex(path[i].id)
            #v2 = g.getVertex(path[i+1].id)
            #distance+=v1.getWeight(v2)    
            
        #erro = 100 - (bruteDistance/distance)*100
        #strErro = " [erro = "+"{0:.2f}".format(erro)+"%]"  
        #print("\t\tDistance =",distance,"km's",strErro) 
        #k+=1