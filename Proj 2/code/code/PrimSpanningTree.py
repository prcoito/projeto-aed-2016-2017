﻿#https://interactivepython.org/runestone/static/pythonds/Graphs/PrimsSpanningTreeAlgorithm.html
#import random
#import time
#import re
from Algoritmos import *
#from Ficheiros import *
#from Geradores import *
from Graph import *
from PriorityQueue import *


def PrimsSpanningTree(G,start):
    pq = PriorityQueue()
    for v in G:                            
        v.setDistance(float("inf"))
        v.setPred(None)
        
    start.setDistance(0)
    pq.buildHeap([(v.getDistance(),v) for v in G])
    while not pq.isEmpty():
        currentVert = pq.delMin()
        for nextVert in currentVert.getConnections():
            newCost = currentVert.getWeight(nextVert)
            if nextVert in pq and newCost<nextVert.getDistance():
                nextVert.setPred(currentVert)
                nextVert.setDistance(newCost)
                pq.decreaseKey(nextVert,newCost)