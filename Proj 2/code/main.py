﻿import os
import sys
sys.path.insert(0, os.getcwd()+'\code')

import random
import time
import re
from Algoritmos import *
from Ficheiros import *
from Geradores import *
from Graph import *
from PrimSpanningTree import *
from PriorityQueue import *

def interfaceIni():
    n = str(input("Escolha a versao que deseja utilizar:\n1. Nao optimizada\n2. Optimizada\n3. Sair\n"))
    while n!='3':
        if n == '1':
            interfaceNO()
        elif n == '2':
            interfaceO()
        else:
            print("Escolha uma opcao valida.\n")
        n = str(input("Escolha a versao que deseja utilizar:\n1. Nao optimizada\n2. Optimizada\n3. Sair\n"))
    return

def interfaceO():
    os.system('cls' if os.name == 'nt' else 'clear')  
     
    otimizado = True
    g = None
    
    n = str(input("Escolha um numero das opcoes seguintes:\n1. Gerar grafo\n2. Carregar grafo\n3. Salvar Grafo\n4. Calcular o menor caminho\n5. Imprimir grafo\n6. Sair\nOpcao: "))
    while n!='6':
        if n=='1':
            g = menugera()
        elif(n=='2'):
            tempg = readGraphFromFile()
            if tempg !=None:
                g = tempg
        elif n=='3':
            if (g == None):
                print("Ainda nao foi gerado nenhum grafo\n")
            else:
                saveGraphToFile(g)
        elif n=='4':
            if (g == None ):
                print("Ainda nao foi gerado nenhum grafo\n")
            else:
                opt = str(input("Pretende escolher manualmente o vertice inicial? (sim/nao)\n"))
                if (opt.lower() == "sim"):
                    i = vertSelect(g)
                elif (opt.lower() == "nao"):
                    i = geraVerticeInicial(g)
                selectA(g, i, otimizado)

        elif n=='5':
            if g!=None:
                print(g)
            else:
                print("Nao existe nenhum grafo para imprimir")
        else:
            print("Escolha uma opcao valida\n")
        n = str(input("Escolha um numero das opcoes seguintes:\n1. Gerar grafo\n2. Carregar grafo\n3. Salvar Grafo\n4. Calcular o menor caminho\n5. Imprimir grafo\n6. Sair\nOpcao: "))

    os.system('cls' if os.name == 'nt' else 'clear')        
    return    

def interfaceNO():
    os.system('cls' if os.name == 'nt' else 'clear') 
    otimizado = False
    g = None
    n = str(input("Escolha um numero das opcoes seguintes:\n1. Gerar grafo\n2. Carregar grafo\n3. Salvar Grafo\n4. Calcular o menor caminho\n5. Imprimir grafo\n6. Sair\nOpcao: "))
    while n!='6':
        if n=='1':
            g = menugera() 
        elif n=='2':
            tempg = readGraphFromFile()
            if tempg !=None:
                g = tempg
        elif n=='3':
            if (g == None):
                print("Ainda nao foi gerado nenhum grafo\n")
            else:
                saveGraphToFile(g)
        elif n=='4':
            if g == None:
                print("Ainda nao foi gerado nenhum grafo\n")
            else:
                opt = str(input("Pretende escolher manualmente o vertice inicial? (sim/nao)\n"))
                if (opt.lower() == "sim"):
                    i = vertSelect(g)
                    selectA(g, i, otimizado)
                else:
                    i = geraVerticeInicial(g)
                    selectA(g, i, otimizado)

        elif n=='5':
            if g==None:
                print("Nao existe nenhum grafo\n")
            else:
                print(g)
        else:
            print("Escolha uma opcao valida\n")
        n = str(input("Escolha um numero das opcoes seguintes:\n1. Gerar grafo\n2. Carregar grafo\n3. Salvar Grafo\n4. Calcular o menor caminho\n5. Imprimir grafo\n6. Sair\nOpcao: "))

    os.system('cls' if os.name == 'nt' else 'clear')    
    return

def menugera():
    os.system('cls' if os.name == 'nt' else 'clear')  
    erro=True
    g=None
    while erro:
        n = str(input("Escolha um numero do tipo de grafo:\n1. Distancia de A para B igual a distancia de B para A\n2. Distancia de A para B diferente da distancia de B para A\n3. Voltar\nOpcao: "))
        if(n=='1'):
            v= str(input("Indique o número de vertices: "))
            if isInt(v):
                if int(v)>0:
                    erro=False
                    g=geraGrafoCompleto1(int(v))
                else:
                    print("Escolha um numero valido\n")
            else:
                print("Escolha um numero valido\n")
        elif n=='2':
            v= str(input("Indique o número de vertices: "))
            if isInt(v):
                if int(v)>0:
                    erro=False
                    g=geraGrafoCompleto2(int(v))
                else:
                    print("Escolha um numero valido\n")
            else:
                print("Escolha um numero valido\n")		
        elif n=='3':
            erro = False
        else:
            print("Escolha um numero valido\n")
            
    os.system('cls' if os.name == 'nt' else 'clear')     
    return g

def selectA(g, i, otimizado):
    os.system('cls' if os.name == 'nt' else 'clear')  
    
    n = str(input("Selecione o algortimo que pretende usar:\n1. Nearest neighbour\n2. Força Bruta\n3. Heuristica usando uma Spanning Tree minima\n4. Voltar \n"))
    while n!='4':
        if n=='1':
            if otimizado:
                NearestNeighbour1(g,i)
            else:
                NearestNeighbour2(g,i)
        elif n=='2':
            bruteForce(g,i)
        elif n=='3':
            if otimizado:
                getSpanningTreePathDistance(g,i)
            else:
                getSpanningTreePathDistanceO(g,i)
        else:
            print("Por favor, selecione uma opcao valida.\n")
        n = str(input("Selecione o algortimo que pretende usar:\n1. Nearest neighbour\n2. Força Bruta\n3. Heuristica usando uma Spanning Tree minima\n4. Sair \n"))  
    os.system('cls' if os.name == 'nt' else 'clear')  
    return

def vertSelect(g):
    erro = True
    n = str(input("Indique o vertice inicial: "))
    while erro:
        if not isInt(n):
            print("Por favor, selecione um vértice valido")
            n = str(input("Indique o vertice inicial: "))
        elif int(n)>g.numVertices:
            print("Por favor, selecione um vértice valido")
            n = str(input("Indique o vertice inicial: "))
        else:
            erro=False
    return g.getVertex(int(n))


if __name__ == "__main__":
    interfaceIni()
    sys.path.remove(os.getcwd()+'\code')
    
 
    